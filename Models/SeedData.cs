﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MvcMovie.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMovie.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcMovieContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcMovieContext>>()))
            {
                // Look for any Movies and any Actors
                if (context.Movie.Any() && context.Actor.Any())
                {
                    return;   // DB has been fully seeded
                }

                // Checks if no Movies recorded
                if(!context.Movie.Any())
                {
                    AddMovies(context);
                }

                // Checks if no Actors recorded
                if (!context.Actor.Any())
                {
                    AddActors(context);
                }

                context.SaveChanges();
            }
        }

        // Adds seeded data for movies if there are no movies in the database
        private static void AddMovies(MvcMovieContext context)
        {
            context.Movie.AddRange(
                new Movie
                {
                    Title = "When Harry Met Sally",
                    ReleaseDate = DateTime.Parse("1989-2-12"),
                    Genre = "Romantic Comedy",
                    Rating = "R",
                    Price = 7.99M
                },

                new Movie
                {
                    Title = "Ghostbusters ",
                    ReleaseDate = DateTime.Parse("1984-3-13"),
                    Genre = "Comedy",
                    Rating = "R",
                    Price = 8.99M
                },

                new Movie
                {
                    Title = "Ghostbusters 2",
                    ReleaseDate = DateTime.Parse("1986-2-23"),
                    Genre = "Comedy",
                    Rating = "R",
                    Price = 9.99M
                },

                new Movie
                {
                    Title = "Rio Bravo",
                    ReleaseDate = DateTime.Parse("1959-4-15"),
                    Genre = "Western",
                    Rating = "R",
                    Price = 3.99M
                }
                
            );
        }

        // Adds seeded data for Actors if there are no Actors in the database
        private static void AddActors(MvcMovieContext context)
        {
            context.Actor.AddRange(
                    new Actor
                    {
                        FirstName = "Cory",
                        LastName = "Serlin",
                        DateOfBirth = DateTime.Parse("1997-11-7")
                    },

                    new Actor
                    {
                        FirstName = "Evlogi",
                        LastName = "Ihsan",
                        DateOfBirth = DateTime.Parse("1960-11-7")
                    }
                
                
                );
        }
    }
}
